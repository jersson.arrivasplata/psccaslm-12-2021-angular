import { AuthGuard } from './guard/auth.guard';
import { UsersComponent } from './dashboard/users/users.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ProductsComponent } from './dashboard/products/products.component';


const routes: Routes = [
  { path: '', redirectTo: 'login',pathMatch:'full' },
  { path: 'login', component: LoginComponent },
  {
    canActivate:[AuthGuard],
    path: 'dashboard/profile',
    component: ProfileComponent
  },
  {
    canActivate:[AuthGuard],
    path: 'dashboard/users',
    component: UsersComponent
  },
  {
    canActivate:[AuthGuard],
    path: 'dashboard/products',
    component: ProductsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
