import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Router } from "@angular/router";
import { User } from '../dashboard/users/models/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private serviceUrl = environment.serviceUrl;
  constructor(private http: HttpClient, private route:Router) { }

  login(email, password) {
    const body = {
      email: email,
      password: password
    };
    return this.http.post(`${this.serviceUrl}auth/login/admin`, body);
  }

  register(user: User) {
    return this.http.post(`${this.serviceUrl}auth/register`, user);
  }

  logout(){
    localStorage.removeItem('token');
    this.route.navigate(['/']);
    return this.http.post(`${this.serviceUrl}auth/user`, {});
  }

  me(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'x-access-token': localStorage.getItem('token')
      })
    };
    return this.http.get(`${this.serviceUrl}auth/user`, httpOptions);
  }

  isLogged(){
    return !!localStorage.getItem('token');
  }
}
