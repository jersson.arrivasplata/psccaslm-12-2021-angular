import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { map } from "rxjs/operators";
import { User, } from "./../dashboard/users/models/user";
import { newUser } from "./../dashboard/users/models/newUser";
import { RolesEnum } from '../config/roles.enum';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private serviceUrl = environment.serviceUrl;
  public selectedUser: User;
  public newUser: newUser;
  public users: User[]=[];

  constructor(private http: HttpClient) { 
    this.selectedUser = new User();
    this.newUser = new newUser();
  }
  
  public httpOptionsToken = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'x-access-token': localStorage.getItem('token')
    })
  };

  getAllUsers(){
    return this.http.post<User[]>(`${this.serviceUrl}auth/user/all`,{},this.httpOptionsToken)
  }
  postUser(User: User) {
    User.roles=RolesEnum.SUPER_ADMIN;
    return this.http.post(`${this.serviceUrl}auth/register`, User,this.httpOptionsToken);
  }

  editUser(User: User) {
    return this.http.post(`${this.serviceUrl}auth/user/update`, User, this.httpOptionsToken);
  }

  deleteUser(email: string) {
    var body = JSON.parse(`{"email":"${email}"}`);
    return this.http.post(`${this.serviceUrl}auth/user/remove`,body, this.httpOptionsToken);
  }

  setRol(email: string,rol: string){
    var body = JSON.parse(`{"email":"${email}" , "roles":"${rol}" }`);
    return this.http.post(`${this.serviceUrl}users/set-roles/`,body, this.httpOptionsToken);
  }

  
  logout(){
    return this.http.post(`${this.serviceUrl}auth/logout`, {});
  }
}
