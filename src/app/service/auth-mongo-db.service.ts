import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthMongoDbService {
  private serviceUrl = environment.serviceUrl;
  constructor(private http: HttpClient) { }

  login(email, password) {
    const body = {
      email: email,
      password: password
    };
    return this.http.post(`${this.serviceUrl}mongodb/auth/login`, body);
  }

  register(email, password) {
    const body = {
      email: email,
      password: password
    };
    return this.http.post(`${this.serviceUrl}mongodb/auth/register`, body).toPromise();
  }

  logout(){
    return this.http.post(`${this.serviceUrl}mongodb/auth/user`, {});
  }

  specials(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'x-access-token': localStorage.getItem('token')
      })
    };
    return this.http.get(`${this.serviceUrl}mongodb/auth/specials`, httpOptions);
  }
}
