import { TestBed } from '@angular/core/testing';

import { AuthMongoDbService } from './auth-mongo-db.service';

describe('AuthMongoDbService', () => {
  let service: AuthMongoDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthMongoDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
