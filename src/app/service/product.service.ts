import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { map } from "rxjs/operators";
import { User, } from "./../dashboard/users/models/user";
import { newUser } from "./../dashboard/users/models/newUser";
import { RolesEnum } from '../config/roles.enum';
import { Product } from '../dashboard/products/models/product';
import { newProduct } from '../dashboard/products/models/newProduct';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private serviceUrl = environment.serviceUrl;
  public selectedProduct: Product;
  public newProduct: newProduct;
  public products: Product[]=[];
  constructor(private http: HttpClient) {
    this.selectedProduct = new Product();
    this.newProduct = new newProduct();
  }

  public httpOptionsToken = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'x-access-token': localStorage.getItem('token')
    })
  };

  getAllProducts(){
    return this.http.post<Product[]>(`${this.serviceUrl}auth/product/all`,{},this.httpOptionsToken)
  }
  postProduct(Product: Product) {

    let input = new FormData();
    input.append('name', Product.name);
    input.append('abstract', Product.abstract);
    input.append('description', Product.description);
    input.append('productImage', Product.file,Product.file.name);
    //input.append('productImage',this.image,this.image.name);
    input.append('type', Product.type);
    input.append('nutritional',JSON.stringify(this.newProduct.nutritional.filter(x => x.name!=='') ));
    input.append('image', Product.file.name);
    return this.http.post(`${this.serviceUrl}auth/product/add`, input, {
      headers: new HttpHeaders({
        'x-access-token': localStorage.getItem('token')
      })
    });
  }


  editProduct(Product: Product) {
     // Append files to the virtual form.

    let input = new FormData();
    input.append('id', Product.id);
    input.append('name', Product.name);
    input.append('abstract', Product.abstract);
    input.append('description', Product.description);
    input.append('type', Product.type);

    input.append('productImage', Product.file,Product.file.name);
    //input.append('file[0]', Product.file,Product.file.name);
    //input.append('productImage',this.image,this.image.name);
    input.append('image', Product.file.name);
    input.append('nutritional',JSON.stringify(this.selectedProduct.nutritional.filter(x => x.name!=='') ));

    return this.http.post(`${this.serviceUrl}auth/product/update`, input,  {
      headers: new HttpHeaders({
        'x-access-token': localStorage.getItem('token')
      })
    });
  }

  deleteProduct(id: string) {
    var body = JSON.parse(`{"id":"${id}"}`);
    return this.http.post(`${this.serviceUrl}auth/product/remove`,body, this.httpOptionsToken);
  }

  setTags(id: string,tag: string){
    var body = JSON.parse(`{"id":"${id}" , "tags":"${tag}" }`);
    return this.http.post(`${this.serviceUrl}auth/product/tags`,body, this.httpOptionsToken);
  }


}
