import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { UsersComponent } from './dashboard/users/users.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { FiltroPipe } from './dashboard/users/pipes/filtro.pipe';
import { OnlynumberDirective } from './directive/onlynumber.directive';
import { ProductsComponent } from './dashboard/products/products.component';
import { FiltroProductPipe } from './dashboard/products/pipes/filtro.pipe';
import { FileInputValueAccessorDirective } from './dashboard/products/directives/file-input-value-accessor.directive';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    ProfileComponent,
    ProductsComponent,
    HeaderComponent,
    FiltroPipe,
    FiltroProductPipe,
    OnlynumberDirective,
    FileInputValueAccessorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private authService:AuthService){}
}
