import { AuthMongoDbService } from './../../service/auth-mongo-db.service';
import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Route, Router } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user ={
    email:'',
    password:''
  }
  @Input()
  signInError: boolean;
  //public email:string='jerssonjgar@gmail.com';
  //public password:string='123456789';

  constructor(public authService:AuthService,
    private authMongoDbService:AuthMongoDbService,
    private router: Router
    ) { }

  ngOnInit(): void {
  
    if(localStorage.getItem('token')){
      console.log('sesion activa')
      this.router.navigate(['dashboard/profile']);
    }else{
      localStorage.getItem('no sesion')
    }
  }

  login(){
    this.authService.login(this.user.email,this.user.password)
    .subscribe((response:any)=>{
      console.log(response);
      this.signInError=false;
      localStorage.setItem('token',response.token);
      this.router.navigate(['dashboard/users']);
    },error=>{
      console.log(error)
      this.signInError=true;
    })
    /*
    this.authMongoDbService.login(this.user.email,this.user.password)
    .subscribe((response:any)=>{
      console.log(response)
      localStorage.setItem('token',response.token)
    },error=>{
      console.log(error)
    },()=>{

    })*/
  }
}
