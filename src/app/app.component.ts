import { Component, OnInit, DoCheck } from '@angular/core';
import { halfmoon } from 'halfmoon';
import { AuthService } from 'src/app/service/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit, DoCheck {
  title = 'psccaslm-angular';
  
  public token;
  public identity;
  constructor(public authService:AuthService) { }

  private DaysArray = ['Miércoles', 'Jueves', 'Viernes', 'Sábado','Domingo', 'Lunes', 'Martes']
  private date = new Date();
  public hour : any;
  public minute: String;
  public second: String;
  public ampm: String;
  public day: String;
  

  ngOnInit(){
    setInterval(()=>{
      const date = new Date();
      this.updateDate(date);
    },1000);
    console.log(this.date.getDate());
    this.day = this.DaysArray[this.date.getDate()];
  }
  private updateDate(date: Date) {
    const hours = date.getHours();
    this.ampm = hours> 12 ? 'PM' : 'AM';
    this.hour = hours % 12;
    this.hour  = this.hour ? this.hour : 12;
    this.hour = this.hour <10 ? '0' + this.hour : this.hour;

    const minutes = date.getMinutes();
    this.minute = minutes<10 ? '0'+ minutes : minutes.toString();

    const seconds = date.getSeconds();
    this.second = seconds<10 ? '0' + seconds: seconds.toString();
  }
  ngDoCheck(){
    this.identity = localStorage.getItem('token');
    this.token =  localStorage.getItem('token');
  }
}
