import { AuthMongoDbService } from './../../service/auth-mongo-db.service';
import { AuthService } from 'src/app/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { Halfmoon } from 'src/app/Utils/Halfmoon';
import { UserService } from 'src/app/service/user.service';
import { NgForm } from '@angular/forms';
import { User } from './models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UserService]
})
export class UsersComponent implements OnInit {
  
  public page: number = 0;
  public search: string;
  public selectedOption: any = "name";

  public criteria = [
    { option: "Nombres", value: "name" },
    { option: "Correo", value: "email" },
    { option: "Rol", value: "roles" }
  ];


  consoleLog(option: any) {
    this.selectedOption = option;
  }

  constructor(private authService: AuthService,
    private authMongoDbService: AuthMongoDbService,
    public userService: UserService) { }

  ngOnInit(): void {
    this.getAllUsers();
  }

  registerOrEdit(form?: NgForm) {
    console.log(form.value);
    if (form.value.email) {
      this.userService.editUser(form.value).subscribe((res) => {
        this.toastAlertSuccess("Registro actualizado correctamente");
        this.getAllUsers();
       // this.clearForm(form);
        this.closeModal('modal-2')
      }, error => {
        this.toastAlertFiled(`Registro incorrecto: ${error.error.message}`);
      });
    } 
  }
  deleteUser(email: string){
    console.log(email)
    this.userService.deleteUser(email).subscribe((res)=>{
      this.toastAlertSuccess("Registro eliminado correctamente");
      this.getAllUsers();
    },error=>{
      this.toastAlertFiled(`No se pudo eliminar : ${error.error.message}`);
    });
  }
  register(form?: NgForm) {
    console.log(form.value);
    
      this.userService.postUser(form.value).subscribe((res) => {
        this.toastAlertSuccess("Registro correcto");
       // this.clearForm(form);
        this.getAllUsers();
        this.closeModal('modal-3');
      }, error => {
        this.toastAlertFiled(`Registro incorrecto: ${error.error.message}`);
      });

  }
  setRol(email:string,rol:any){
    var roles:string = rol.target.value;
    roles=roles.toUpperCase();
    this.userService.setRol(email,roles).subscribe((res) => {
      this.toastAlertSuccess("Registro actualizado correctamente");
      this.getAllUsers();
    }, error => {
      this.toastAlertFiled(`No se pudo Actualizar: ${error.error.message}`);
    });
  }
  logout() {
    this.userService.logout()
      .subscribe(response => {
        console.log(response)
      }, error => {

      }, () => {

      })
  }

  openModal(id: string, user: User) {
    Halfmoon.halfmoon.toggleModal(id);
    this.userService.selectedUser = user
  }
  openModalRegister(id: string) {
    Halfmoon.halfmoon.toggleModal(id);
  }
  closeModal(id) {
    Halfmoon.halfmoon.toggleModal(id);
  }

  nextPage() {
    this.page += 3;
  }
  prevPage() {
    if (this.page > 0) this.page -= 3;
  }

  onSearchUser(search: string) {
    this.page = 0
    this.search = search;
  }

  toastAlertSuccess(messajeToast: string) {
    // Built-in function
    Halfmoon.halfmoon.initStickyAlert({
      content: messajeToast,      // Required, main content of the alert, type: string (can contain HTML)
      title: "Mensaje del sistema" ,     // Optional, title of the alert, default: "", type: string
      alertType: "alert-success"
    })
  }
  toastAlertFiled(messajeToast: string) {
    // Built-in function
    Halfmoon.halfmoon.initStickyAlert({
      content: messajeToast,      // Required, main content of the alert, type: string (can contain HTML)
      title: "Mensaje del sistema" ,     // Optional, title of the alert, default: "", type: string
      alertType: "alert-danger"
    })
  }

 /* clearForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.userService.selectedUser = new User();
    }
  }*/

  getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(user => {
        this.userService.users = user;
        console.log(this.userService.users);
      })

  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  aphOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    console.log(charCode);
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 173)) {
      return true;
    }
    return false;

  }

}
