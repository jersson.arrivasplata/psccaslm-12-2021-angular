import { Pipe, PipeTransform } from '@angular/core';
import { FetchAllUserResponse } from '../interfaces/user.interfaces';
import { User } from '../models/user';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(Users: User[], page: number = 0,search:string = '',selectedOption:string = ''): User[] {
    
    if(search.length===0&& typeof page !=='undefined' && typeof Users !=="undefined"){
      return Users.slice(page,page+10).filter(user => user.status);
    }
    if(selectedOption.length > 0){
      return Users.filter(user => user[selectedOption].toUpperCase().includes(search.toUpperCase()) && user.status);
    }else {
      return Users.slice(page,page+10).filter(user => user.status);
    }
    
  }

}
