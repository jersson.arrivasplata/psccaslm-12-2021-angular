export class newUser {
    constructor(id = "",password = "",email = "",roles = "", name = "") {
      this.id = id;
      this.password = password;
      this.email = email;
      this.roles = roles;
      this.name = name;
    }
  
    id:          string;
    password:     string;
    status:       boolean;
    roles?:        string;
    email:        string;
    name:         string;
  }