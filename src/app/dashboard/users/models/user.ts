export class User {
    constructor(id = "",email = "",roles = "", name = "") {
      this.id = id;
      this.email = email;
      this.roles = roles;
      this.name = name;
    }
  
    id:          string;
    status:       boolean;
    roles?:        string;
    email:        string;
    password:     string;
    name:         string;
  }

 
