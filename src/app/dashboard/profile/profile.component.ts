import { AuthService } from 'src/app/service/auth.service';
import { AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit,AfterViewInit {
  public name: string;
  public email: string;
  constructor(private authService: AuthService) { }



  private DaysArray = ['Miércoles', 'Jueves', 'Viernes', 'Sábado','Domingo', 'Lunes', 'Martes']
  private date = new Date();
  public hour : any;
  public minute: String;
  public second: String;
  public ampm: String;
  public day: String;
  

  ngOnInit(){
    setInterval(()=>{
      const date = new Date();
      this.updateDate(date);
    },1000);
    this.day = this.DaysArray[this.date.getDate()];
  }
  private updateDate(date: Date) {
    const hours = date.getHours();
    this.ampm = hours> 12 ? 'PM' : 'AM';
    this.hour = hours % 12;
    this.hour  = this.hour ? this.hour : 12;
    this.hour = this.hour <10 ? '0' + this.hour : this.hour;

    const minutes = date.getMinutes();
    this.minute = minutes<10 ? '0'+ minutes : minutes.toString();

    const seconds = date.getSeconds();
    this.second = seconds<10 ? '0' + seconds: seconds.toString();
  }

  ngAfterViewInit(): void {
    this.me()
  }
  me() {
    this.authService.me()
      .subscribe((response: any) => {
        console.log(response)
        this.name = response.name;
        this.email = response.email;
      }, error => {

      }, () => {

      })
  }
}
