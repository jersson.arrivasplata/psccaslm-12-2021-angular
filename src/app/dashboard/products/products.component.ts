import { Component, OnInit } from '@angular/core';
import { Halfmoon } from 'src/app/Utils/Halfmoon';
import { NgForm } from '@angular/forms';
import { Product } from './models/product';
import { ProductService } from 'src/app/service/product.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  providers: [ProductService]
})
export class ProductsComponent implements OnInit {

  public page: number = 0;
  public search: string;
  public selectedOption: any = "name";
  public serviceImage = environment.service;
  files: any[];
  public image:File;

  public criteria = [
    { option: "Nombre", value: "name" },
    { option: "Tags", value: "tags" }
  ];


  consoleLog(option: any) {
    this.selectedOption = option;
  }

  constructor(
    public productService: ProductService) { }

  ngOnInit(): void {
    this.getAllProducts();
  }

  fileChoosen(event: any){
    if(event.target.value){
      this.image = <File>event.target.files[0];
    }
  }
  registerOrEdit(form?: NgForm) {
    console.log(form.value);
    if (form.value.name) {
      console.group( "Form View-Model" );
      console.log( "Name:", "Prueba" );
      console.groupEnd();

      //this.image

      this.productService.editProduct(form.value).subscribe((res) => {
        this.toastAlertSuccess("Registro actualizado correctamente");
        this.getAllProducts();
       // this.clearForm(form);
        this.closeModal('modal-2')
      }, error => {
        this.toastAlertFiled(`Registro incorrecto: ${error.error.message}`);
      });
    }
  }
  deleteProduct(id: string){
    console.log(id)
    this.productService.deleteProduct(id).subscribe((res)=>{
      this.toastAlertSuccess("Registro eliminado correctamente");
      this.getAllProducts();
    },error=>{
      this.toastAlertFiled(`No se pudo eliminar : ${error.error.message}`);
    });
  }
  register(form?: NgForm) {
    console.log(form.value);

      this.productService.postProduct(form.value).subscribe((res) => {
        this.toastAlertSuccess("Registro correcto");
       // this.clearForm(form);
        this.getAllProducts();
        this.closeModal('modal-3');
      }, error => {
        this.toastAlertFiled(`Registro incorrecto: ${error.error.message}`);
      });

  }
  setRol(id:string,tags:any){
    this.productService.setTags(id,tags).subscribe((res) => {
      this.toastAlertSuccess("Registro actualizado correctamente");
      this.getAllProducts();
    }, error => {
      this.toastAlertFiled(`No se pudo Actualizar: ${error.error.message}`);
    });
  }


  openModal(id: string, product: Product) {
    Halfmoon.halfmoon.toggleModal(id);
    this.productService.selectedProduct = product
  }
  openModalRegister(id: string) {
    this.productService.newProduct.nutritional=[{
      name:'',
      value:10
    }];
    Halfmoon.halfmoon.toggleModal(id);
  }
  closeModal(id) {
    Halfmoon.halfmoon.toggleModal(id);
  }

  nextPage() {
    this.page += 3;
  }
  prevPage() {
    if (this.page > 0) this.page -= 3;
  }

  onSearchProduct(search: string) {
    this.page = 0
    this.search = search;
  }

  toastAlertSuccess(messajeToast: string) {
    // Built-in function
    Halfmoon.halfmoon.initStickyAlert({
      content: messajeToast,      // Required, main content of the alert, type: string (can contain HTML)
      title: "Mensaje del sistema" ,     // Optional, title of the alert, default: "", type: string
      alertType: "alert-success"
    })
  }
  toastAlertFiled(messajeToast: string) {
    // Built-in function
    Halfmoon.halfmoon.initStickyAlert({
      content: messajeToast,      // Required, main content of the alert, type: string (can contain HTML)
      title: "Mensaje del sistema" ,     // Optional, title of the alert, default: "", type: string
      alertType: "alert-danger"
    })
  }


  getAllProducts() {
    this.productService.getAllProducts()
      .subscribe(products => {
        this.productService.products = products;
        console.log(this.productService.products);
      })

  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  aphOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    console.log(charCode);
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 173)) {
      return true;
    }
    return false;

  }

  onFileChange(event){
    this.files = event.target.files;
    console.log(event);
  }

  addEditNutritional(){
    this.productService.selectedProduct.nutritional.push({
      name:'',
      value:10
    });
  }
  addNutritional(){
    this.productService.newProduct.nutritional.push({
      name:'',
      value:10
    });
  }

  stringToArray(data:string){
    return JSON.parse(data);
  }
}
