export class Product {
    constructor(id = "",name = "",abstract = "", image = "", type="", description = "", file=null, nutritional: {
      name: string;
      value: number;
    }[]= [{
      name:'',
      value:10
    }]) {
      this.id = id;
      this.name = name;
      this.abstract = abstract;
      this.image = image;
      this.description = description;
      this.file=file;
      this.type = type;
      this.nutritional = nutritional;
    }

    id:           string;
    name:         string;
    abstract:     string;
    image:        string;
    description:  string;
    status:       boolean;
    file?: File | null;
    type: string;
    nutritional?: {
      name: string;
      value: number;
    }[]
  }


