import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../models/product';
@Pipe({
  name: 'filtroProduct'
})
export class FiltroProductPipe implements PipeTransform {

  transform(Products: Product[], page: number = 0,search:string = '',selectedOption:string = ''): Product[] {
    
    if(search.length===0&& typeof page !=='undefined' && typeof Products !=="undefined"){
      return Products.slice(page,page+10).filter(product => product.status);
    }
    if(selectedOption.length > 0){
      return Products.filter(product => product[selectedOption].toUpperCase().includes(search.toUpperCase()) && product.status);
    }else {
      return Products.slice(page,page+10).filter(product => product.status);
    }
    
  }

}
