// Archivo de configuracion que sirve para servir los archivos del proyecto
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

const app = express();
const allowedExt = ['.js', '.ico', '.css', '.gif', '.png', '.jpg', '.woff2', '.woff', '.ttf', '.svg'];

//Configuracion de acceso a servidores
app.use(function(req, res, next) {

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', '*');

    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/static', express.static(path.join(__dirname, 'dist/psccaslm-angular')));

app.use((req, res) => {
    if (allowedExt.filter(ext => req.url.indexOf(ext) > 0).length > 0) {
        res.sendFile(`${__dirname}/dist/psccaslm-angular${req.url}`)
    } else {
        res.sendFile(__dirname + '/dist/psccaslm-angular/index.html');
    }
});

const port = process.env.PORT || '9000';
app.set('port', port);

const server = http.createServer(app);

// listen (start app with node server.js) ==============================================
server.listen(port, '0.0.0.0', () => console.log(`API running on localhost:${port}`));
